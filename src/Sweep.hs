{-# LANGUAGE CPP, FlexibleContexts, GeneralisedNewtypeDeriving, TypeFamilies #-}
module Sweep where
import Prelude hiding (lines)
import Data.Bifunctor
import Control.Monad.State.Strict
import Control.Monad.Identity
import qualified Data.Tree.AVL as AVL
import Debug.Trace
import Data.List ((\\))
import Data.Either
import Data.Maybe (maybeToList)
import Data.Number.IReal.Powers
import Data.Bits (xor)
import Data.Heap (Heap, Entry)
import qualified Data.Heap as Heap
import Data.Foldable (foldl')

import Approx
import Geometry

data Event =
  Event
    { adds :: [Object] -- ^ list of objects to add
    , removes :: [Object] -- ^ list of objects to remove
    }
  deriving (Show)

instance Semigroup Event where
  (Event a b)  <> (Event x y) = Event (a <> x) (b <> y)

instance Monoid Event where
  mempty = Event [] []

type EventQueue = Heap (Entry Point Event)-- PSQ.OrdPSQ (Point) (Point) (Event)

type Result = (Int, Int, Int, Int) -- Left boundary, bottom left corner, bottom boundary, inside
type Zip = AVL.ZAVL Object
type BinTree = AVL.AVL Object
data Object =
    L {-# UNPACK #-} !Line
  | C {-# UNPACK #-} !Circle !Bool
  deriving (Eq, Show)

data SweepState =
  SweepState
    { swLine :: !(AVL.AVL Object)
    , swQ :: !EventQueue
    , swR:: !Result
    , swP :: !Point
    }
  deriving Show

newtype Sweep s a = Sweep {unSweep :: StateT SweepState Identity a}
  deriving (Functor, Applicative, Monad, MonadState SweepState)

runSweep :: Sweep s a -> (a, Result)
runSweep (Sweep k) =
  second swR $ runIdentity $ runStateT k initSweepState

initSweepState :: SweepState
initSweepState =
  SweepState
    { swLine = AVL.empty
    , swQ = Heap.empty
    , swR = (0, 0, 0, 0)
    , swP = Point 0 0
    }

insertQ :: Point -> Event -> Sweep s ()
insertQ p@(Point x y) e = modify' (\s -> s {swQ = Heap.insert (Heap.Entry p e) (swQ s)})

popQ :: Sweep s (Maybe (Point, Event))
popQ = gets swQ >>= f . Heap.uncons
 where
   f Nothing = return Nothing
   f (Just (Heap.Entry p e, q')) =
     modify (\s -> s{swQ = lastq}) >> return (Just (p, foldl' (<>) e es))
     where
       (es, lastq) = takeWhileEq q'
       takeWhileEq q =
         case Heap.uncons q of
           Nothing -> ([], q)
           Just (Heap.Entry p' e, q') ->
             if p == p' then
               first (e:) (takeWhileEq q')
             else
               ([], q)

cmpY :: Point -> Object -> Ordering
cmpY (Point _ _) (L (Line Inf _ )) = EQ
cmpY (Point x y) (L (Line (Fin m) c)) = compare y (x*m + c) --(x*ma + c*mb)/mb
cmpY (Point x y) c@(C (Circle a b r rs) u)
  | srhs == EQ || srhs == s = if u then compare (sq rhs) lhs else compare lhs (sq rhs)
  | otherwise = if u then LT else GT
  where
    s = if u then GT else LT
    lhs = rs - sq (x-a)--s*sqrt (rs - sq (x-a))
    rhs = y - b
    srhs = compare rhs 0

insertS :: Point -> BinTree -> Object -> BinTree
insertS p@(Point x y) t l1 =
  case AVL.openEither (cmp l1 p) t of
    Left z -> AVL.close $ AVL.fill l1 z
    Right _ -> error ("we already inserted this" ++ show l1 ++ show (AVL.asListL t))
  where
    cmp :: Object -> Point -> Object -> Ordering
    cmp l1 p@(Point x y) l2 =
      compare (slope p l1) (slope p l2)
      <> case (l1, l2) of
           (C c1 u1, C c2 u2) ->
             if u1 == u2 then
               if u1 then compare (rsq c1) (rsq c2) else compare (rsq c2) (rsq c1)
             else
               compare (not u1) (not u2)
           (C _ u, L _) -> if u then LT else GT
           (L _, C _ u) -> if u then GT else LT

#ifndef LEFT
#define LEFT 0
#endif
#ifndef RIGHT
#define RIGHT inf
#endif

topB, rightB, leftB, bottomB :: R
topB = inf
rightB = RIGHT
leftB = LEFT
bottomB = 0

addResult :: Point -> Sweep s ()
addResult (Point x y) = modify' (\s -> s{swR = f (swR s)})
  where
    f (a,b,c,d) = (a',b',c',d')
     where
      cLeft = compare x leftB
      cRight = compare x rightB
      cBottom = compare y bottomB
      a' = if cLeft == EQ && cBottom == EQ then 1+a else a
      b' = if cLeft == EQ && cBottom /= LT then 1+b else b
      c' = if cLeft /= LT && cBottom == EQ then 1+c else c
      d' = if cLeft /= LT && cBottom /= LT then 1+d else d

slope :: Point -> Object -> (R, Slope)
slope _ (L (Line Inf _)) = (2, Inf)
slope _ (L (Line m _)) = (0, m)
slope (Point x y) (C (Circle a b _ _) u)
  | y == b = (if u `xor` (x<a) then -1 else 1, Inf)
  | otherwise = (0, Fin $ -1*(x-a)/(y-b))

reverseC :: Sweep s (Maybe Zip, Maybe Object, [Object])
reverseC = do
  p@(Point _ y) <- gets swP
  t <- gets swLine
  case AVL.tryOpenLE (\l2 -> cmpY p l2 <> GT) t of
    Nothing -> return (Nothing, AVL.getCurrent <$> AVL.tryOpenL t, [])
    Just z -> do
      let justAfter = AVL.getCurrent <$> AVL.tryMoveR z
      let (Right (t', zBefore, ls)) = until isRight pop (Left (z, []))
      let justBefore = zBefore
      modify (\s -> s{swLine=t'})
      return (justBefore, justAfter, ls)
        where
          --pop :: Either (Zip n, [Object n]) (Zip n, [Object n]) -> Either (Zip n, [Object n]) (Zip n, [Object n])
          pop (Left (z, xs)) =
            if cmpY p e /= EQ then
              Right (AVL.close z, Just z, xs)
            else
              case AVL.tryDelMoveL z of
                Just z' -> Left (z', e:xs)
                Nothing -> Right (AVL.delClose z, Nothing, e:xs)
            where
              e = AVL.getCurrent z
          pop r@(Right _) = r

intersect (L l1) (L l2) = intersectLL l1 l2
intersect (C c u) (L l) = filter (above c u) $ intersectCL c l
intersect (L l) (C c u) = filter (above c u) $ intersectCL c l
intersect (C c1 u1) (C c2 u2)
  | a c1 == a c2 && b c1 == b c2 = []
  | otherwise = filter (above c2 u2) $ filter (above c1 u1) $ intersectCC c1 c2

above (Circle _ b _ _) True (Point _ y) = y >= b
above (Circle _ b _ _) False (Point _ y) = y <= b

processEvent :: Point -> Event -> Sweep s ()
processEvent p (Event ns ds) = do
  modify (\s -> s{swP = p})
  (zBefore, justAfter, cs) <- reverseC -- pop off elements at p
  when (atleast 2 (cs ++ ns))  $ addResult p -- Is this an intersection point? (careful about circles here)
  t <- gets swLine
  let justBefore = AVL.getCurrent <$> zBefore
  -- insert new elements, and those which we've popped off and won't be removed (this restores order).
  let elems = (cs ++ ns) \\ ds
  let subTree = foldl' (insertS p) AVL.empty elems
  let t' = case zBefore of
        Just z -> AVL.close $ AVL.insertTreeR z subTree
        Nothing -> AVL.join subTree t
  modify (\s -> s{swLine = t'})
  -- Now we add future intersections
  let potential = maybeToList justBefore ++ AVL.asListL subTree ++ maybeToList justAfter
  let intersections = filter (\x -> x > p && xCoord x <= rightB) $ join $ zipWith intersect potential (tail potential)
  mapM_ (`insertQ` Event [] []) intersections -- we add future events
    where
      atleast :: Int -> [Object] ->  Bool
      atleast 0 _ = True
      atleast _ [] = False
      atleast n (L _:xs) = atleast (n-1) xs
      atleast n (C a b:xs) = atleast (n-1) $ filter (notagain $ C a b) xs
      notagain (C a _) (C b _) = a /= b
      notagain _ _ = True

processNextEvent :: Sweep s Bool
processNextEvent = do
  x <- popQ
  case x of
    Just (p, e) -> processEvent p e >> return True
    Nothing -> return False

inf :: Num n => n
inf = 10^1000 -- I am certain that no point has co-ordinates greater than this. No circle has a radius greater than 16. And the minimum difference between the slope of any two line is around 1e-7.

initialise :: [Object] -> Sweep s ()
initialise ls = mapM_ f $ zipWith (\ _ x -> x) [1..] ls
  where
    f y = mapM_ (uncurry insertQ) (boundryIntersectionsToEvent y $ boundryIntersections y)

inBounds :: Point -> Bool
inBounds (Point x y) =  x >= leftB && y >= bottomB && x <= rightB && y <= topB

leftBoundry, bottomBoundry, topBoundry, rightBoundry :: Object
leftBoundry = L (line (Point leftB 0) (Point leftB 1))
bottomBoundry = L (line (Point 0 bottomB) (Point 1 bottomB))
topBoundry = L (line (Point 0 topB) (Point 1 topB))
rightBoundry = L (line (Point rightB 0) (Point rightB 1))

boundryIntersections :: Object -> [Point]
boundryIntersections (L (Line Inf c)) = if inBounds (Point c 0) then [Point c 0, Point c inf] else []
boundryIntersections l@(L (Line (Fin m) c)) =
  fnub $ filter inBounds $ [leftBoundry, bottomBoundry, rightBoundry, topBoundry] >>= intersect l
boundryIntersections c@(C (Circle a b r rs) d) =
  fnub $ filter inBounds
    (Point a (b+s*r):Point (a-r) b:Point (a+r) b:([leftBoundry, bottomBoundry, rightBoundry] >>= intersect c))
  where
    s = if d then 1 else -1

boundryIntersectionsToEvent :: Object -> [Point] -> [(Point, Event)]
boundryIntersectionsToEvent o [] = []
boundryIntersectionsToEvent o [x] = [(x, Event [o] [o])]
boundryIntersectionsToEvent o xs
  | length xs > 4 = undefined
  | otherwise = [(a, Event [o] []), (b, Event [] [o])] ++ boundryIntersectionsToEvent o (reverse rs)
  where
    rs = tail $ init xs
    a = head xs
    b = last xs

go :: [Object] -> Sweep s ()
go ls = initialise ls >> go' 0
  where
    go' 0 = get >>= (\s -> traceShow (swR s, swP s) (return ())) >> go' (2000::Int)
    go' n = processNextEvent >>= (\b -> when b $ go' (n-1))
